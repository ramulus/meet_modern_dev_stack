class DupaksController < ApplicationController

  def index
    @dupaks = Dupak.all
  end

  def show
    @dupak = Dupak.find(params[:id])
  end

  def new

  end
  def create
    @dupak = Dupak.new(dupak_params)
 
    @dupak.save
    redirect_to @dupak 
  end

  private
  def dupak_params
    params.require(:dupak).permit(:dupaka_name, :with_who)
  end
end
