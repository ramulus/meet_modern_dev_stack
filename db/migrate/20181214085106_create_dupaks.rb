class CreateDupaks < ActiveRecord::Migration[5.2]
  def change
    create_table :dupaks do |t|
      t.string :dupaka_name
      t.text :with_who

      t.timestamps
    end
  end
end
