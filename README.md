## SetUp

### Setup environment ###
* [ Install vagrant ](http://docs.vagrantup.com/v2/installation/)
* [ Install VirtualBox ](https://www.virtualbox.org/wiki/Downloads)
* [ Install Ansible ](http://docs.ansible.com/intro_installation.html)

### Install ansible ruby role:

```sh
ansible-galaxy install ivaldi.ruby
```

### Create development machine with Vagrant

```sh
vagrant up
```

You may receive "No guest IP was given to the Vagrant core NFS helper. This is an internal error that should be reported as a bug."
error message on first run.

To continue perform

```sh
vagrant reload
```

## Roadmap

### What

Set up dev env locally without drag and drop technology. Vagrant, Ansible, Ansible-galaxy, Docker, Docker-Compose.

### Whyyyy?

I would like to split elastic training for:

* Showing elastic usage
* Play elastic query - the funny part
* Elastic internals, which are not needed for start, but they are must have in terms of PROD
* All of this will base on Vagrant and/or Docker

### My motivation do set up dev env in separate local env

* Dev set up sometimes is not easy. Here it is done once.
* In  my host machien I can have ruby or PG in different version
* Can develop multiple projects, which can have different requirments
* If I break something e.g. DB I can easy clean up.
* I do not whant to affect my host machine 

### Agenda?

[Lets create it](https://app.mindmup.com/map/_v2/824b4050ff2611e8b03f4f35cbe90e2b)

### Vagrant

* 'Full' virtualization
* Providers:
    * VirtualBox,
    * Hyper-V,
    * Docker,
* BSD systems e.g. FreeBSD

### Vagrant set up

* Multiple machines - elastic example
* Sync folder
* Maintainable provisioning - Ansible
* Lets try to create our own machine

### Ansible

* Ansible
* Ansible-glaxy
* Ansible playbook - lets focus on that
* Validation:

```sh
ansible-playbook ansible/development/development.yml --check
```

### Docker

* Posgress app example
* PROD app example.
